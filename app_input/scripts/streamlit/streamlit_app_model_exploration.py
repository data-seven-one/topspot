# importing general models
import os
import sys

# importing desired packages for python
import pandas as pd
import numpy as np
import streamlit as st
import math
import shap
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

CWD = os.path.abspath('.')
sys.path.append(CWD + '/scripts')

try:
    import helper_functions as helper
except:
    print('there was an error importing the modules')

# display settings for output
pd.options.display.max_columns = 50
pd.options.display.max_rows = 100

storing_model_path = CWD + '/models/all/model.sav'
X_train_path = CWD + '/models/all/X_train.parquet'
saving_path_all = CWD + '/models/all/'



def st_shap(plot, height=None):
    shap_html = f"<head>{shap.getjs()}</head><body>{plot.html()}</body>"
    components.html(shap_html, height=height)

# Load the data from parquet file on hard drive
@st.cache(allow_output_mutation=True)
def load_everything():
    model_all = helper.load_model(storing_model_path)
    X_train_all = pd.read_parquet(X_train_path)
    #shap_all = helper.make_shap_value(model_all, X_train_all)
    return model_all, X_train_all#, shap_all


def build_app():
    # Streamlit Layout
    #st.beta_set_page_config(layout="wide")
    helper._max_width_()
    #st.markdown('<font style="font-family: Helvetica; font-size:23pt">  **Model and Feature Analysis** </font>', unsafe_allow_html=True)

    model_load_state = st.markdown('<font style="font-family: Helvetica; font-size:10pt" > :hourglass: Loading models and data... </font>', unsafe_allow_html=True)

    #model_all, X_train_all, shap_all = load_everything()
    model_all, X_train_all = load_everything()


    list_of_assets = list(X_train_all.asset_name.drop_duplicates())
    model_load_state.markdown('<font style="font-family: Helvetica; font-size:10pt" > :heavy_check_mark: Loading models and data...done! </font>', unsafe_allow_html=True)


    # Streamlit Sidebar Layout
    st.sidebar.markdown('<font style="font-family: Helvetica; font-size:18pt" > Chosing model to analyze </font>', unsafe_allow_html=True)
    st.sidebar.markdown('<font style="font-family: Helvetica; font-size:12pt" > Switch between model trained with all data or asset specific data </font>', unsafe_allow_html=True)

    model_picker = st.sidebar.selectbox('Select what model to use:', ['All assets', 'Model trained with individual assets'])
    graph_picker = st.sidebar.selectbox('Select a graph to see:', ['Detailed Shapley Values', 'Feature Importance', 'Specific Feature Attribution'])

    if model_picker != 'All assets':
        compare_picker = st.sidebar.radio('Do you want to compare assets?', options=['Yes', 'No'], index=1)
        if compare_picker == 'Yes':
            assets_to_compare = st.sidebar.multiselect('Assets to compare:', list_of_assets, default=['MYDAYS', 'VERIVOX'])

            for i in range(1, math.ceil(len(assets_to_compare)/2)+1):
                start_list = (i-1)*2
                end_list = i*2
                if end_list > len(assets_to_compare):
                    list_dist = assets_to_compare[start_list:]
                else:
                    list_dist =  assets_to_compare[start_list:end_list]

                cols = st.beta_columns(len(list_dist))

                cols[0].write('Model for {}'.format(str.lower(list_dist[0])))
                if len(cols) > 1:
                    cols[1].write('Model for {}'.format(str.lower(list_dist[1])))
        st.write('Not implemented yet')

    else:
        if graph_picker == 'Detailed Shapley Values':
            #image = helper.make_detailed_shap_plot(shap_all, X_train_all, saving_path_all+'/detailed_shap.jpg')
            image=mpimg.imread(saving_path_all+'/detailed_shap.jpg')
            st.image(image)

        elif graph_picker == 'Feature Importance':
            #image = helper.make_feature_importance_shap_plot(shap_all, X_train_all, saving_path_all+'/feature_importance_shap.jpg')
            image=mpimg.imread(saving_path_all+'/feature_importance_shap.jpg')
            st.image(image)

        elif graph_picker == 'Specific Feature Attribution':
            # feature = st.sidebar.selectbox('Chose a specific feature to observe', list(X_train_all.columns))
            # image =  helper.make_dependence_shap_plot(shap_all, X_train_all, feature, saving_path_all+'/feature_{}.jpg'.format(feature))
            # image=mpimg.imread(saving_path_all+'/feature_importance_shap.jpg')
            #
            # st.image(image)
            st.write('Not implemented yet')
