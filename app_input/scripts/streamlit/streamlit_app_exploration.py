# importing general models
import os
import sys

# importing desired packages for python
import pandas as pd
import numpy as np
import seaborn as sn
import streamlit as st
import plotly.express as px
import plotly.graph_objects as go
from io import BytesIO
import datetime
import base64
from pyathena import connect
import io
from sklearn.preprocessing import LabelBinarizer
from scipy.stats import pearsonr
import pingouin as pg
import datetime as dt
import matplotlib.pyplot as plt
#import xlswriter
# global settings
CWD = os.path.abspath('.')
sys.path.append(CWD + '/scripts')

try:
    import data_cleaning as cleaning
    import helper_functions as helper
except:
    print('there was an error importing the modules')

# display settings for output
pd.options.display.max_columns = 50
pd.options.display.max_rows = 100



# set up path to access raw data and folder to store any results
@st.cache(allow_output_mutation=True)
def load_data(CWD):
    folderpath_processed_data = CWD + '/data/02_processed'
    df = pd.read_parquet(folderpath_processed_data + '/01_spot_report_data_cleaned.parquet')

    return df


def make_pivot_table(df, show_total_numbers=False, index_columns=['is_prime_time'], value_columns=['audience','total_react_linear']):
    """ creates a pivot table for the given daraframe
    using the audience and total react linear as the aggregation values
    from those values the conversion rate (defined as reactions/audience)
    is calculated as the final value to show in the pivot table

    input: df = dataframe to make the pivot table out of
           show_total_numbers = boolean that defines whether to show the
                                total numbers for the audience or reactions.
                                if == False then only conversion will be shown
           index_columns = single column or set of two columns to be used as
                           columns to group by (if nothing is chosen default is
                           set to prime time)
           value_columns = columns to be aggregated. default is audience and linear react

    output: pivot_color = pivot table with color scheme based on values

    """
    # create pivot table with internal pandas function
    pivot = pd.pivot_table(
                            data=df,
                            index=index_columns,
                            values=value_columns,
                            aggfunc= np.sum
                          )

    pivot = pivot.reset_index()
    # convert the value columns to integer
    for i in value_columns:
        pivot[i] = pivot[i].astype('int')
    # if both audience and reactions are present caluclate the conversion
    if 'audience' in value_columns and 'total_react_linear' in value_columns:
         pivot['conversion_rate[%]'] = (pivot['total_react_linear']/pivot['audience'])*100
         pivot['conversion_rate[%]'] = pivot['conversion_rate[%]'].apply(lambda x: round(x,3))
    # if weekday is the only column to aggregate by, then sort it by the weekdays
    if ('weekday' in index_columns) and len(index_columns)==1:
        cats = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        pivot = pivot.groupby(index_columns).agg({'audience':np.sum, 'total_react_linear':np.sum, 'conversion_rate[%]':np.mean}).reindex(cats).reset_index(drop=False)

    # else sort by the values in the index columns
    else:
        pivot = pivot.sort_values(by=index_columns)

    # if the total number checkmark is checked, then show everything, else drop the columns
    # that contain the audience and the total react values
    if show_total_numbers:
        pivot_show = pivot.copy()
    elif ~ show_total_numbers:
        pivot_show = pivot.copy().drop(columns=['audience','total_react_linear'])

    # color the pivot table based on the values of the conversion
    cm = sn.light_palette("green", as_cmap=True)
    pivot_color = pivot_show.style.background_gradient(cmap=cm, subset=['conversion_rate[%]'])
    return pivot_color


def create_histogram(df_hist, additional_filter, filter_keep):
    """ creates a histogram for given dataframe using a specific metric

    input: df_hist = dataframe to make the pivot table out of
           additional_filter =  metric to use as X-axis for the histogram
           filter_keep = filter histogram by one value of the metric
                         and show that data
           grouped = boolean that defines whether to show the
                     total numbers for the audience or reactions.
                     if == False then only conversion will be shown

    output: pivot_color = pivot table with color scheme based on values

    """

    #df_hist['react_per_100_audience'] = 100*(df_hist['total_react_linear']/df_hist['audience'])
    df_hist['is_prime_time'] = np.where(df_hist['is_prime_time']==1, 'prime', 'no_prime')
    df_filtered_hist = df_hist.copy()
    # Explore Target variable total react total_react_linear

    df_grouped = df_filtered_hist.groupby(additional_filter, as_index=False).agg({'react_per_100_audience':[np.mean, np.std, np.max]})
    if additional_filter == 'weekday':
        cats = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        df_grouped = df_filtered_hist.groupby(additional_filter).agg({'react_per_100_audience':[np.mean, np.std, np.max]}).reindex(cats).reset_index(drop=False)
    else:
        df_grouped = df_grouped.sort_values(by=additional_filter,ascending=True)
    df_grouped.columns = ["".join(x) for x in df_grouped.columns.ravel()]
    #df_grouped.columns = df_grouped.columns.droplevel(0)
    df_grouped_sorted = df_grouped.rename(
                                            columns={
                                                        'react_per_100_audiencemean': 'average',
                                                        'react_per_100_audiencestd':'standard_deviation',
                                                        'react_per_100_audienceamax':'maximum'
                                                    }
                                            )
    fig_filter = go.Figure()
    fig_filter.add_trace(go.Histogram(
        x= df_grouped_sorted[additional_filter],
        y= df_grouped_sorted['average'],
        histfunc='avg',
        name='Avg Reactions / 100 aud',
        nbinsx=len(filter_metrics)+1
    ))
    fig_filter.add_trace(go.Scatter(
        name='Std dev',
        mode='markers',
        x=df_grouped_sorted[additional_filter], y=df_grouped_sorted['average'],
        error_y=dict(type='data', array=df_grouped_sorted['standard_deviation'], color='purple', thickness=1.5, width=3),
        marker=dict(color='black', size=5, opacity=0.75)
    ))

    fig_filter.update_layout(
                                title='Average reactions per 100 audience by {}'.format(additional_filter),
                                xaxis_title=additional_filter,
                                yaxis=dict(rangemode='nonnegative'),
                                yaxis_title="Avg Reactions / 100 aud",
                                #legend_title="Legend Title",
                                autosize=False,
                                width=600,
                                height=400,
                                font=dict(
                                    #family="Courier New, monospace",
                                    size=12,
                                    color="black"
                                )
                             )

    title = 'Histogram for observations where {} is {}'.format(additional_filter, filter_keep)
    df_filtered_hist = df_filtered_hist[df_filtered_hist[additional_filter]==filter_keep]
    cutoff = 0.9
    fig = px.histogram(
                        np.clip(df_filtered_hist['react_per_100_audience'], -0.1, cutoff),
                        x = 'react_per_100_audience',
                        nbins=400,
                        histnorm='percent'
                       )
    fig.update_layout(
                        title=title,
                        xaxis_title="Reactions per 100 audience",
                        yaxis_title="Distribution in %",
                        autosize=False,
                        width=600,
                        height=400,
                        font=dict(
                                    #family="Courier New, monospace",
                                    size=12,
                                    color="black"
                                 )
                     )
    return fig_filter, fig, df_grouped_sorted


def calculate_pvalues(df):
    df = df.dropna()._get_numeric_data()
    dfcols = pd.DataFrame(columns=df.columns)
    pvalues = dfcols.transpose().join(dfcols, how='outer')
    for r in df.columns:
        for c in df.columns:
            pvalues[r][c] = round(pearsonr(df[r], df[c])[1],4)
    return pvalues


def create_correlation_to_target_value(df_corr,feature_column_list, target_column='react_per_100_audience'):
    print(feature_column_list)
    if target_column in feature_column_list:
        feature_column_list.remove(target_column)
    list_of_dfs = []
    for i in feature_column_list:
        try:
            df_pg = pg.corr(x=df_corr[i], y=df_corr[target_column])
            df_pg.index = [i]
            list_of_dfs.append(df_pg)
        except:
            print('correlation did not work for {}'.format(i))
    df_complete_corr = pd.concat(list_of_dfs, ignore_index=False)
    df_complete_corr = df_complete_corr[[
                                            'n',
                                            'r',
                                            'CI95%',
                                            'p-val',
                                            'BF10'
                                        ]].rename(
                                                    columns={
                                                                'n':'# sample',
                                                                'r': 'correlation coefficient',
                                                                'CI95%': '95% confidence interval',
                                                                'p-val': 'p value',
                                                                'BF10': 'Bayes Factor'
                                                            }
                                                  )
    return df_complete_corr


def create_correlation_heatmap(df_corr, feature_column_list, additional_filter_corr, filter_keep_corr):
    """
    Creates a correlation matrix of all numerical features
    input:
            included_feature: a string of an encoded categorical
                              feature to include in correlation matrix
    output:
            a figure of a heatmap with the correlations
    """
    if additional_filter_corr != 'None':
        df_corr = df_corr[df_corr[additional_filter_corr]==filter_keep_corr]
    if len(df_corr)>15000:
        # st.write('Only using last 15.000 entries to create correlation Heatmap')
        df_corr = df_corr.sort_values(by='ad_timestamp', ascending=False).head(15000)
    # find categorical columns



    df_sample_all_corr = df_corr[feature_column_list]
    # calculate p value dataframe
    p_value_df = calculate_pvalues(df_sample_all_corr)
    # calculate all correlation details df
    df_full_correlation = create_correlation_to_target_value(df_corr,feature_column_list, target_column='react_per_100_audience')
    # create correlation heatmap
    corrs = df_sample_all_corr.corr()
    mask = np.zeros_like(corrs)
    mask[np.triu_indices_from(mask)] = True

    #plt.title('Correlation matrix')
    #
    # correlation_matrix_all = df_sample_all_corr.corr()
    # mask = np.zeros_like(correlation_matrix_all)
    # mask[np.triu_indices_from(mask)] = True
    with sn.axes_style("white"):
        f, ax = plt.subplots(figsize=(10, 8))
        ax = sn.heatmap(corrs, cmap='Spectral_r', mask=mask, square=True, vmin=-.4, vmax=.4, annot=True, fmt='.1g')

    return ax.get_figure(), p_value_df, df_full_correlation


def create_scatter_plot(df_scatter, feature):
    """
    """
    df_scatter['conversion_rate[%]'] = (df_scatter['total_react_linear']/df_scatter['audience'])*100
    df_scatter['conversion_rate[%]'] = df_scatter['conversion_rate[%]'].apply(lambda x: round(x,3))
    df_scatter['is_prime_time'] = np.where(df_scatter['is_prime_time']==1, 'prime', 'no_prime')

    fig_scatter = go.Figure()
    fig_scatter.add_trace(go.Scatter(
        name='Std dev',
        mode='markers',
        x=df_scatter[feature], y=df_scatter['conversion_rate[%]'],
        marker=dict(color='black', size=5, opacity=0.75)
    ))

    fig_scatter.update_layout(
                                title='Conversion Rate Scatter Plot for feature {}'.format(feature),
                                xaxis_title=feature,
                                yaxis=dict(rangemode='nonnegative'),
                                yaxis_title=" Conversion Rate [Reactions/100 aud]",
                                #legend_title="Legend Title",
                                autosize=False,
                                width=700,
                                height=450,
                                font=dict(
                                    #family="Courier New, monospace",
                                    size=12,
                                    color="black"
                                )
                             )

    return fig_scatter


def build_app():
    # Streamlit Layout
    #st.beta_set_page_config(layout="wide")
    helper._max_width_()
    #st.markdown('<font style="font-family: Helvetica; font-size:23pt"> :tv: **Explorative Spot Analysis** </font>', unsafe_allow_html=True)
    st.markdown('<font style="font-family: Helvetica; font-size:11pt"> *Conversion is calculated as ratio between audience and reactions* </font>', unsafe_allow_html=True)

    data_load_state = st.markdown('<font style="font-family: Helvetica; font-size:10pt" > :hourglass: Loading data... </font>', unsafe_allow_html=True)
    # Load the data from parquet file on hard drive
    df_raw = load_data(CWD)
    data_load_state.markdown('<font style="font-family: Helvetica; font-size:10pt" > :heavy_check_mark: Loading data...done! </font>', unsafe_allow_html=True)

    # Checkbox whether to show the raw dataframe
    st.markdown('---')

    # Streamlit Sidebar Layout
    st.sidebar.markdown('<font style="font-family: Helvetica; font-size:18pt" > Analysis specifications :pencil2: </font>', unsafe_allow_html=True)
    st.sidebar.markdown('<font style="font-family: Helvetica; font-size:12pt" > Change settings for explorative analysis </font>', unsafe_allow_html=True)
    #raw_data_check = st.sidebar.checkbox('Show raw Data')

    # Radio button whether to show & compare to different dates or one analysis
    comparison = st.sidebar.radio('Compare two dates?', ['No', 'Yes'])

    # When chosing one view only create graphs and layout
    if comparison =='No':
        left, right = st.beta_columns((1,2))
        # Pick a date to show the analysis for
        # filter dataframe based on the desired constrains and use the desired frame for the rest of the analysis
        date_picker = st.sidebar.selectbox('Select Date Range', ['All', 'Pick Quarter', 'Pick Date Range', 'Last [X] months'])
        if date_picker == 'All':
            # Include all data in the analysis
            df= df_raw
        elif date_picker == 'Pick Quarter':
            # Ask the user to pick a specific set of quater(s) to include in the analysis
            quater_select = st.sidebar.multiselect('Quarters to be included', [1,2,3,4], [1,2,3,4])
            # add an extra column that specifies the quarter for the original dataset
            df_raw['quarter'] = df_raw.ad_timestamp.apply(lambda x: x.quarter)
            # only keep the columns that are relevant for the chosen quarter(s)
            df = df_raw[df_raw.quarter.isin(quater_select)].drop(columns='quarter')
        elif date_picker == 'Pick Date Range':
            # Ask the user to define a start and end date to include in the analysis
            start_date = st.sidebar.date_input('Start Date', pd.Timestamp(df_raw.ad_timestamp.min()))
            end_date = st.sidebar.date_input('End Date', pd.Timestamp(df_raw.ad_timestamp.max()))
            # only keep the columns that are relevant for the chosen time period
            df = df_raw[(df_raw.ad_timestamp > (start_date - pd.DateOffset(days=0))) & (df_raw.ad_timestamp < (end_date + pd.DateOffset(days=1)))]
        elif date_picker == 'Last [X] months':
            # Ask the user to define a number of months to be included in the analysis
            last_months = st.sidebar.slider('Chose the number of months to go back', min_value=1, max_value=20, value=6, step=1)
            # Calculate the desired date by subtracting it from todays date
            cut_date = pd.Timestamp.today() - pd.DateOffset(day=1, months=last_months)
            st.sidebar.write('Including all data starting 1st of {} {}'.format(cut_date.month_name(), cut_date.year))
            # only keep the columns that are relevant for the chosen months
            df = df_raw[(df_raw.ad_timestamp >= cut_date) ]

        # Check if raw data is selected to be shown
        # If so, then write the necessary columns as a pandas dataframe in the interface
        # if raw_data_check:
        #     df['real_position_im_block'] = df.apply(lambda x: str(x.tv_position_im_block)+' von ' + str(x.tv_spotanzahl_im_block), axis=1)
        #     df_show = df.copy()[[
        #                                     'ad_timestamp',
        #                                     'weekday',
        #                                     'hauptwerbetraeger_name',
        #                                     'real_position_im_block',
        #                                     'asset_name',
        #                                     'motiv_name',
        #                                     'spot_id',
        #                                     'tv_insel',
        #                                     'tv_spotlaenge',
        #                                     'audience',
        #                                     'total_react_linear',
        #                                     ]]
        #     df_show['total_react_linear'] = df_show['total_react_linear'].apply(lambda x: round(x,0))
        #     right.write(df_show.sort_values(by='ad_timestamp', ascending=True))
        #     right.markdown('---')


        # Streamlit Sidebar selection of what to see
        show_what = st.sidebar.selectbox('Select a graph to see', ['Pivot table', 'Histogram', 'Correlation Matrix', 'Scatter Plot'])

        # If Pivot table is selected for analysis
        if show_what == 'Pivot table':
            # define a list of columns that can be selected as index columns
            # to use for aggregation of the pivot table
            pivot_index_options = [
                                    'weekday',
                                    'is_prime_time',
                                    'hour',
                                    'hauptwerbetraeger_name',
                                    'relative_spot_position',
                                    'tv_spotlaenge',
                                    'asset_name'
                                  ]
            pivot_index_second_option = ['None'] + pivot_index_options.copy()
            first_index = left.selectbox('Select a column to use for pivot table', pivot_index_options)
            pivot_index_second_option_updates = pivot_index_second_option#.remove(first_index)
            second_index = left.selectbox('Select a secondary column to use for pivot table', pivot_index_second_option_updates)
            show_total_numbers = left.checkbox('Show numbers for audience and reactions')
            if second_index != 'None':
                index_columns = [first_index, second_index]
            elif second_index == 'None':
                index_columns = [first_index]
            pivot_show = make_pivot_table(
                                                df=df,
                                                show_total_numbers=show_total_numbers,
                                                index_columns=index_columns,
                                                value_columns=['audience','total_react_linear']
                                            )
            right.write(pivot_show)

        # If Histogram is selected for analysis
        elif show_what == 'Histogram':
            # List of features that can be used for the histogram
            features_to_include =  [
                                            'hour',
                                            'weekday',
                                            'is_prime_time',
                                            'hauptwerbetraeger_name',
                                            'tv_position_im_block',
                                            'tv_spotanzahl_im_block',
                                            'relative_spot_position',
                                            'tv_spotlaenge',
                                            'asset_name',
                                            'motiv_name'
                                        ]
            select_options = features_to_include
            # select an option for the specific metric defined above to show a histrogram for
            # that filtered option
            additional_filter = left.selectbox('Filter histogram by additional metric', options=select_options)
            title = 'Histogram for all observations'
            filter_metrics = list(df_raw[additional_filter].drop_duplicates().sort_values())
            filter_keep =  left.selectbox('Select desired filtering option', options=filter_metrics)
            grouped = left.checkbox('Display grouped raw data')
            fig_hist, fig_filtered_hist, df_grouped_sorted = create_histogram(df, additional_filter, filter_keep)
            right.plotly_chart(fig_hist)
            right.plotly_chart(fig_filtered_hist)
        elif show_what == 'Correlation Matrix':

            features_to_filter = ['None', 'asset_name', 'hauptwerbetraeger_name', 'weekday']
            additional_filter_corr = left.selectbox('Filter correlation by additional metric', options=features_to_filter)
            if additional_filter_corr == 'None':
                filter_keep_corr = 'None'
            else:
                filter_metrics_corr = list(df_raw[additional_filter_corr].drop_duplicates().sort_values())
                filter_keep_corr =  left.selectbox('Select desired filtering option', options=filter_metrics_corr)
            df_corr = df.copy()
            feature_column_list =  [
                                            'hour',
                                            'is_prime_time',
                                            'hauptwerbetraeger_name',
                                            'tv_position_im_block',
                                            'tv_spotanzahl_im_block',
                                            'relative_spot_position',
                                            'tv_spotlaenge',
                                            'audience',
                                            'day_time',
                                            'spot_position_at_edge',
                                            'react_per_100_audience'
                                        ]
            correlation_heatmap_general, p_value_df, df_full_correlation = create_correlation_heatmap(df_corr, feature_column_list, additional_filter_corr, filter_keep_corr)

            if additional_filter_corr != 'None':
                right.write('Correlation Heatmap with Pearson’s correlation coefficient where {} is {}'.format(additional_filter_corr, filter_keep_corr))
            else:
                right.write('General Correlation Heatmap with Pearson’s correlation coefficient')
            right.write(correlation_heatmap_general)
            if left.checkbox('Show P-Value Dataframe'):
                right.write(p_value_df)
            if left.checkbox('Show Full Correlation Details Dataframe'):
                right.write(df_full_correlation)
        elif show_what == 'Scatter Plot':
            # List of features that can be used for the histogram
            features_to_include =  [
                                            'hour',
                                            'weekday',
                                            'is_prime_time',
                                            'hauptwerbetraeger_name',
                                            'tv_position_im_block',
                                            'tv_spotanzahl_im_block',
                                            'relative_spot_position',
                                            'tv_spotlaenge',
                                            #'audience',
                                            'asset_name',
                                            'motiv_name'
                                            #'total_react_linear'
                                        ]
            select_options = features_to_include
            # select an option for the specific metric defined above to show a histrogram for
            # that filtered option
            additional_filter = left.selectbox('Chose additional metric for Scatterplot', options=select_options)
            title = 'Scatter Plot for all observations'
            df_scatter = df.copy()
            fig_scatter = create_scatter_plot(df_scatter, additional_filter)
            right.plotly_chart(fig_scatter)
            #right.plotly_chart(fig_filtered_hist)

    if comparison =='Yes':
        left, middle, right = st.beta_columns((1,2,2))
        date_picker = st.sidebar.selectbox('Select Date Range', ['Pick Quarter', 'Pick Date Range'])

        if date_picker == 'Pick Quarter':
            df_raw['quarter'] = df_raw.ad_timestamp.apply(lambda x: x.quarter)
            quater_select1 = st.sidebar.multiselect('Quarters to be included [Timespan 1]', [1,2,3,4], [1,2])
            df1 = df_raw[df_raw.quarter.isin(quater_select1)].drop(columns='quarter')
            quater_select2 = st.sidebar.multiselect('Quarters to be included [Timespan 2]', [1,2,3,4], [3,4])
            df2 = df_raw[df_raw.quarter.isin(quater_select2)].drop(columns='quarter')

        elif date_picker == 'Pick Date Range':
            start_date1 = st.sidebar.date_input('Start Date [Timespan 1]', pd.Timestamp(df_raw.ad_timestamp.min()))
            end_date1 = st.sidebar.date_input('End Date [Timespan 1]', pd.Timestamp('2019-12-31'))
            df1 = df_raw[(df_raw.ad_timestamp > (start_date1 - pd.DateOffset(days=0))) & (df_raw.ad_timestamp < (end_date1 + pd.DateOffset(days=1)))]
            start_date2 = st.sidebar.date_input('Start Date [Timespan 2]', pd.Timestamp('2020-01-01'))
            end_date2 = st.sidebar.date_input('End Date [Timespan 2]', pd.Timestamp(df_raw.ad_timestamp.max()))
            df2 = df_raw[(df_raw.ad_timestamp > (start_date2 - pd.DateOffset(days=0))) & (df_raw.ad_timestamp < (end_date2 + pd.DateOffset(days=1)))]
        # if raw_data_check:
        #     df = pd.concat([df1, df2],ignore_index=True).drop_duplicates()
        #     df['real_position_im_block'] = df.apply(lambda x: str(x.tv_position_im_block)+' von ' + str(x.tv_spotanzahl_im_block), axis=1)
        #     df_show = df.copy()[[
        #                                     'ad_timestamp',
        #                                     'weekday',
        #                                     'hauptwerbetraeger_name',
        #                                     'real_position_im_block',
        #                                     'asset_name',
        #                                     'motiv_name',
        #                                     'spot_id',
        #                                     'tv_insel',
        #                                     'tv_spotlaenge',
        #                                     'audience',
        #                                     'total_react_linear',
        #                                     ]]
        #     df_show['total_react_linear'] = df_show['total_react_linear'].apply(lambda x: round(x,0))
        #     st.write(df_show.sort_values(by='ad_timestamp', ascending=True))
        #     st.markdown('---')

        show_what = st.sidebar.selectbox('Select a graph to see', ['Pivot table', 'Histogram', 'Correlation Matrix', 'Scatter Plot'])
        if show_what == 'Pivot table':

            pivot_index_options = [
                                    'weekday',
                                    'is_prime_time',
                                    'hour',
                                    'hauptwerbetraeger_name',
                                    'relative_spot_position',
                                    'tv_spotlaenge',
                                    'asset_name'
                                  ]
            pivot_index_second_option = ['None'] + pivot_index_options.copy()
            first_index = left.selectbox('Select a column to use for pivot table', pivot_index_options)
            pivot_index_second_option_updates = pivot_index_second_option#.remove(first_index)
            second_index = left.selectbox('Select a secondary column to use for pivot table', pivot_index_second_option_updates)
            show_total_numbers = left.checkbox('Show numbers for audience and reactions')
            if second_index != 'None':
                index_columns = [first_index, second_index]
            elif second_index == 'None':
                index_columns = [first_index]
            middle.markdown('<font style="font-family: Helvetica; font-size:23pt"> **Timespan 1** </font>', unsafe_allow_html=True)
            pivot_show1 = make_pivot_table(
                                                df=df1,
                                                show_total_numbers=show_total_numbers,
                                                index_columns=index_columns,
                                                value_columns=['audience','total_react_linear']
                                            )
            middle.write(pivot_show1)

            right.markdown('<font style="font-family: Helvetica; font-size:23pt"> **Timespan 2** </font>', unsafe_allow_html=True)
            pivot_show2 = make_pivot_table(
                                        df=df2,
                                        show_total_numbers=show_total_numbers,
                                        index_columns=index_columns,
                                        value_columns=['audience','total_react_linear']
                                    )
            right.write(pivot_show2)

        elif show_what == 'Histogram':

            features_to_include =  [
                                            'hour',
                                            'weekday',
                                            'is_prime_time',
                                            'hauptwerbetraeger_name',
                                            'tv_position_im_block',
                                            'tv_spotanzahl_im_block',
                                            'relative_spot_position',
                                            'tv_spotlaenge',
                                            #'audience',
                                            'asset_name',
                                            'motiv_name'
                                            #'total_react_linear'
                                        ]
            select_options = features_to_include
            additional_filter = left.selectbox('Filter histogram by additional metric', options=select_options)
            title = 'Histogram for all observations'
            filter_metrics = list(df_raw[additional_filter].drop_duplicates().sort_values())
            filter_keep =  left.selectbox('Select desired filtering option', options=filter_metrics)
            grouped = st.checkbox('Display grouped raw data')
            fig_hist1, fig_filtered_hist1, df_grouped_sorted1 = create_histogram(df1, additional_filter, filter_keep)
            fig_hist2, fig_filtered_hist2, df_grouped_sorted2 = create_histogram(df2, additional_filter, filter_keep)

            middle.markdown('<font style="font-family: Helvetica; font-size:23pt"> **Timespan 1** </font>', unsafe_allow_html=True)
            middle.plotly_chart(fig_hist1)
            middle.plotly_chart(fig_filtered_hist1)
            right.markdown('<font style="font-family: Helvetica; font-size:23pt"> **Timespan 2** </font>', unsafe_allow_html=True)
            right.plotly_chart(fig_hist2)
            right.plotly_chart(fig_filtered_hist2)
            if grouped:
                middle.write(df_grouped_sorted1)
                right.write(df_grouped_sorted2)

        elif show_what == 'Correlation Matrix':
            features_to_filter = ['None', 'asset_name', 'hauptwerbetraeger_name', 'weekday']
            additional_filter_corr = left.selectbox('Filter correlation matrix by additional metric', options=features_to_filter)
            if additional_filter_corr == 'None':
                filter_keep_corr = 'None'
            else:
                filter_metrics_corr = list(df_raw[additional_filter_corr].drop_duplicates().sort_values())
                filter_keep_corr =  left.selectbox('Select desired filtering option', options=filter_metrics_corr)

            feature_column_list =  [
                                            'hour',
                                            'is_prime_time',
                                            'hauptwerbetraeger_name',
                                            'tv_position_im_block',
                                            'tv_spotanzahl_im_block',
                                            'relative_spot_position',
                                            'tv_spotlaenge',
                                            'audience',
                                            'day_time',
                                            'spot_position_at_edge',
                                            'react_per_100_audience'
                                        ]
            correlation_heatmap_general1, p_value_df1, df_full_correlation1 = create_correlation_heatmap(df1, feature_column_list, additional_filter_corr, filter_keep_corr)
            correlation_heatmap_general2, p_value_df2, df_full_correlation2 = create_correlation_heatmap(df2, feature_column_list, additional_filter_corr, filter_keep_corr)

            if additional_filter_corr != 'None':
                left.write('Correlation Heatmap with Pearson’s correlation coefficient where {} is {}'.format(additional_filter_corr, filter_keep_corr))
            else:
                left.write('General Correlation Heatmap with Pearson’s correlation coefficient')

            p_values_show = left.checkbox('Show P-Value Dataframe')
            full_corr_df_show = left.checkbox('Show Full Correlation Details Dataframe')
            middle.markdown('<font style="font-family: Helvetica; font-size:23pt"> **Timespan 1** </font>', unsafe_allow_html=True)
            middle.write(correlation_heatmap_general1)
            if p_values_show:
                middle.write(p_value_df1)
            if full_corr_df_show:
                middle.write(df_full_correlation1)
            right.markdown('<font style="font-family: Helvetica; font-size:23pt"> **Timespan 2** </font>', unsafe_allow_html=True)
            right.write(correlation_heatmap_general2)
            if p_values_show:
                right.write(p_value_df2)
            if full_corr_df_show:
                right.write(df_full_correlation2)

        elif show_what == 'Scatter Plot':
            # List of features that can be used for the histogram
            features_to_include =  [
                                            'hour',
                                            'weekday',
                                            'is_prime_time',
                                            'hauptwerbetraeger_name',
                                            'tv_position_im_block',
                                            'tv_spotanzahl_im_block',
                                            'relative_spot_position',
                                            'tv_spotlaenge',
                                            #'audience',
                                            'asset_name',
                                            'motiv_name'
                                            #'total_react_linear'
                                        ]
            select_options = features_to_include
            # select an option for the specific metric defined above to show a histrogram for
            # that filtered option
            additional_filter = left.selectbox('Chose additional metric for Scatterplot', options=select_options)

            fig_scatter1 = create_scatter_plot(df1, additional_filter)
            fig_scatter2 = create_scatter_plot(df2, additional_filter)

            middle.markdown('<font style="font-family: Helvetica; font-size:23pt"> **Timespan 1** </font>', unsafe_allow_html=True)
            middle.write(fig_scatter1)
            right.markdown('<font style="font-family: Helvetica; font-size:23pt"> **Timespan 2** </font>', unsafe_allow_html=True)
            right.write(fig_scatter2)

    if st.sidebar.button('Load new data from AWS'):
        with st.spinner('Querying data from AWS ...'):
            cleaningload_and_process_raw_data()
            df_raw = load_data(CWD)
        st.balloons()
