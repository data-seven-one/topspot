# importing general models
import os

# importing desired packages for python
import boto3
import botocore
import pandas as pd
import numpy as np
import warnings
from pyathena import connect
from pandas.core.common import SettingWithCopyWarning
# global settings
CWD = os.path.abspath('.')

print('this is just a test')

# display settings for output
pd.options.display.max_columns = 50
pd.options.display.max_rows = 100

# Filter the warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=SettingWithCopyWarning)

def load_fresh_data(folderpath_raw_data):
    # define path of raw data csv file and load as pandas dataframe
    #filepath_data_spot_report = folderpath_raw_data + '\\spot_report_data_raw.csv'
    # define AWS Database connection details and query athena table
    conn = connect(
                    #s3_staging_dir='s3://aws-athena-query-results-dlusers-473599635758',
                    s3_staging_dir='s3://aws-athena-query-results-default-279939523141/queries/',
                    region_name='eu-central-1'
                   )

    df_spot_report = pd.read_sql("SELECT * FROM topspot_link.fct_spot_report", conn)
    df_spot_report.to_parquet(folderpath_raw_data + '/spot_report_data_raw.parquet')
    # only keep a relevant subset for overview purpose
    columns_to_keep_spot_report = [
                            'partition_date',
                            'ad_timestamp',
                            'weekday',
                            'hour',
                            'is_prime_time',
                            'hauptwerbetraeger_name',
                            'tv_position_im_block',
                            'motiv_id',
                            'motiv_name',
                            'tv_spotlaenge',
                            'tv_spotanzahl_im_block',
                            'spot_id',
                            'tv_insel',
                            'asset_name',
                            'react_per_cost',
                            'conv_per_cost',
                            'audience',
                            'total_react_linear',
                            'total_conv_linear'
                      ]
    df = df_spot_report[columns_to_keep_spot_report]
    return df


def modify_column_types(df):
    df['ad_timestamp'] = df['ad_timestamp'].apply(lambda x: pd.Timestamp(x))
    df['is_prime_time'] = np.where(df['is_prime_time'], 1, 0)
    # Add reactions per 100 audience
    df['react_per_100_audience'] = 100*(df['total_react_linear']/df['audience'])
    # eliminate the last 10 days due to inaccuracies
    df = df[df['ad_timestamp']<(pd.Timestamp.today() -  pd.DateOffset(days=10))]
    return df


def add_spot_position_detail_columns(df):
    # add column for the relative spot position in the block
    df['relative_spot_position'] = df['tv_position_im_block']/df['tv_spotanzahl_im_block']
    bins=[0, 0.2, 0.4, 0.6, 0.8, 1]
    labels_real=[0.2, 0.4, 0.6, 0.8, 1]
    df['relative_spot_position'] = pd.cut(
                                            np.array(df['relative_spot_position']),
                                            bins=bins,
                                            labels=labels_real
                                          ).astype('float')
    labels_edge = [2,1,0,1,2]
    df['spot_position_at_edge'] = pd.cut(
                                            np.array(df['relative_spot_position']),
                                            bins=bins,
                                            labels=labels_edge,
                                            ordered=False
                                          ).astype('float')
    return df


def add_day_time_column(df):
    bins = [0, 2, 10, 15, 19, 24]
    labels = [4, 1, 2, 3, 4]
    df['day_time'] = pd.cut(
                                np.array(df['hour']),
                                bins=bins,
                                labels=labels,
                                ordered=False
                              ).astype('float')
    return df


def load_process_and_store_data(load_new=True):
    # set up path to access raw data and folder to store any results
    folderpath_processed_data = CWD + '/data/02_processed'
    folderpath_raw_data = CWD + '/data/01_raw'
    if load_new==True:
        df = load_fresh_data(folderpath_raw_data)
    else:
        df = pd.read_parquet(folderpath_raw_data + '/spot_report_data_raw.parquet')

    df = modify_column_types(df)
    df = add_spot_position_detail_columns(df)
    df = add_day_time_column(df)
    df.to_parquet(folderpath_processed_data + '/01_spot_report_data_cleaned.parquet')

    return df
