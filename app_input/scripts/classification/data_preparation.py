# importing general models
import os
import sys

# importing desired packages for python
import pandas as pd
import numpy as np
import warnings
from pandas.core.common import SettingWithCopyWarning
from scipy import stats
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=SettingWithCopyWarning)

# global settings
CWD = os.path.abspath('.')
sys.path.append(CWD + '/scripts')

try:
    import data_cleaning as cleaning
except:
    print('there was an error importing the modules')

# display settings for output
pd.options.display.max_columns = 50
pd.options.display.max_rows = 1000

#df = pd.read_parquet(CWD + '/data/02_processed/01_spot_report_data_cleaned.parquet')

def bin_target_value_by_asset(df, bins=[0, 0.25, 0.5, 0.75, 1], labels=['low_performer', 'middle_performer', 'high_performer', 'over_performer']):
    df_class = df.copy()
    assets = (df_class['asset_name'].drop_duplicates())
    list_of_asset_dfs = []
    for asset in assets:
        df_asset = df_class[df_class['asset_name']==asset]
        # filter outliers
        df_asset = df_asset[(np.abs(stats.zscore(df_asset['react_per_100_audience'])) < 3)]

        # Build equal quantiles
        quantile_list = bins
        quantile_labels = labels
        quantiles = df_asset['react_per_100_audience'].quantile(quantile_list)
        if (len(df_asset['react_per_100_audience'].drop_duplicates())) > 4:
            df_asset['performance'] = pd.qcut(
                                                df_asset['react_per_100_audience'],
                                                q=quantile_list,
                                                labels=quantile_labels,
                                                duplicates='drop'
                                              )
            list_of_asset_dfs.append(df_asset)
    df_class_binned = pd.concat(list_of_asset_dfs, ignore_index=True)
    return df_class_binned


def bin_target_value_by_asset_not_percent(df, n_bins=4, labels=['low_performer', 'middle_performer', 'high_performer', 'over_performer'], cut_off_date=pd.Timestamp('2020-01-01')):
    df_class = df.copy()
    df_class = df_class[df_class.ad_timestamp > cut_off_date]

    assets = (df_class['asset_name'].drop_duplicates())
    list_of_asset_dfs = []
    for asset in assets:
        df_asset = df_class[df_class['asset_name']==asset]
        # filter outliers
        df_asset = df_asset[(np.abs(stats.zscore(df_asset['react_per_100_audience'])) < 2.5)]

        # Build equal bins
        if (len(df_asset['react_per_100_audience'].drop_duplicates())) > 4:
            df_asset['performance'] = pd.cut(
                                                df_asset['react_per_100_audience'],
                                                bins=n_bins,
                                                labels=labels,
                                                duplicates='drop',
                                                ordered=False
                                              )
            list_of_asset_dfs.append(df_asset)
    df_class_binned = pd.concat(list_of_asset_dfs, ignore_index=True)
    return df_class_binned


def bin_target_by_spot(df, n_bins=3, labels=['low','middle', 'high'], cut_off_date=pd.Timestamp('2020-01-01')):
    df_class = df.copy()
    df_class = df_class[df_class.ad_timestamp > cut_off_date]

    motives = (df_class['motiv_name'].drop_duplicates())
    list_of_motives_dfs = []
    for motive in motives:
        df_motive = df_class[df_class['motiv_name']==motive]
        # filter outliers
        df_motive = df_motive[(np.abs(stats.zscore(df_motive['react_per_100_audience'])) < 3)]

        # Build equal bins
        if (len(df_motive['react_per_100_audience'].drop_duplicates())) > 4:
            df_motive['performance'] = pd.cut(
                                                df_motive['react_per_100_audience'],
                                                bins=n_bins,
                                                labels=labels,
                                                duplicates='drop',
                                                ordered=False
                                              )
            list_of_motives_dfs.append(df_motive)
    df_class_binned = pd.concat(list_of_motives_dfs, ignore_index=True)
    return df_class_binned
