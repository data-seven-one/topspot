#!/bin/bash
# starts the traffic app and restarts it if crashed

while true; do
    [ -e stopme ] && break
    streamlit run streamlit_overall.py --server.port 8501
done
