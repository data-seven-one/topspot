# importing general models
import os
import sys

# importing desired packages for python
import pandas as pd
import numpy as np
import streamlit as st
import math
import shap
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

CWD = os.path.abspath('.')
sys.path.append(CWD + '/scripts')
sys.path.append(CWD + '/scripts/streamlit')

try:
    import streamlit_app_model_exploration as model_st
    import streamlit_app_exploration as dataex_st
    import helper_functions as helper
except:
    print('there was an error importing the modules')


# Streamlit Layout
st.beta_set_page_config(layout="wide")
helper._max_width_()
title = st.markdown('<font style="font-family: Helvetica; font-size:24pt">  **Chose either Data Explorer or Shapley Value Explorer** </font>', unsafe_allow_html=True)
main_decide = st.sidebar.radio('Chose:', ['Data Explorer', 'Shapley Value Explorer'])


if main_decide == 'Data Explorer':
    title.markdown('<font style="font-family: Helvetica; font-size:24pt">  **Data Explorer** </font>', unsafe_allow_html=True)
    dataex_st.build_app()
elif main_decide == 'Shapley Value Explorer':
    title.markdown('<font style="font-family: Helvetica; font-size:24pt">  **Shapley Value Explorer** </font>', unsafe_allow_html=True)
    model_st.build_app()
