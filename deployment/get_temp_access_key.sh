unset  AWS_SESSION_TOKEN
unset AWS_SECRET_ACCESS_KEY
unset AWS_ACCESS_KEY_ID
if [ $# -eq 1 ]
  then
    echo "Lets GO"
  else
    echo "Parameter missing, profile expected"
    exit 1
fi
profile=$1
echo $profile
caller_ident=$(aws sts get-caller-identity --profile $profile)
caller_arn=$(echo $caller_ident | jq -r .Arn | cut -f1,2 -d'/'| sed -e "s/assumed-//g")
echo $caller_arn
temp_role=$(aws sts assume-role \
                    --role-arn $caller_arn \
                    --role-session-name $profile"cdk"  \
                    --profile $profile)
export AWS_ACCESS_KEY_ID=$(echo $temp_role | jq -r .Credentials.AccessKeyId)
export AWS_SECRET_ACCESS_KEY=$(echo $temp_role | jq -r .Credentials.SecretAccessKey)
export AWS_SESSION_TOKEN=$(echo $temp_role | jq -r .Credentials.SessionToken)
env | grep -i AWS_
#aws s3 ls
