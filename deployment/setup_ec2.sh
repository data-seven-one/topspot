sudo yum install -y python3
alias python=python3
sudo yum install -y yum install python3-devel gcc gcc-c++
sudo yum install -y unzip aws-cli
sudo curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
sudo python3 get-pip.py
pip3 install -r requirements.txt --upgrade --user
pip3 install awscli --upgrade --user

export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_COLLATE=C
export LC_CTYPE=en_US.UTF-8
export LC_MONETARY=de_DE.UTF-8
export LC_TIME=de_DE.UTF-8
