rm -r app_input
mkdir app_input
mkdir app_input/data
#mkdir app_input/data/01_raw
mkdir app_input/data/02_processed
# cp -ri  data/01_raw/spot_report_data_raw.csv app_input/data/01_raw/spot_report_data_raw.csv
cp -ri  data/02_processed/01_spot_report_data_cleaned.parquet app_input/data/02_processed/01_spot_report_data_cleaned.parquet
cp -r scripts app_input/scripts
cp -r models app_input/models
cp -r requirements.txt app_input/requirements.txt
cp -r deployment/setup_ec2.sh app_input/setup_ec2.sh

aws s3 cp app_input/ s3://topspot/app_input/ --recursive
aws s3 cp run_streamlit_on_ec2/ s3://topspot/run_streamlit_on_ec2/ --recursive



# scp -i ec2_connection.pem -r deployment/running_streamlit_windows.sh ec2-user@ec2-18-159-208-47.eu-central-1.compute.amazonaws.com:~/running_streamlit_windows.sh
#scp -i ec2_connection_pem.pem -r deployment/running_streamlit.sh ec2-user@ec2-18-196-157-221.eu-central-1.compute.amazonaws.com:~/running_streamlit.sh
#rm -r app_input
#ssh -i ec2_connection.pem ec2-18-159-208-47.eu-central-1.compute.amazonaws.com
