rm -r app_input
mkdir app_input
mkdir app_input\data
#mkdir app_input\data\01_raw
mkdir app_input\data\02_processed
# cp -ri  data\01_raw\spot_report_data_raw.csv app_input\data\01_raw\spot_report_data_raw.csv
cp -ri  data\02_processed\01_spot_report_data_cleaned.parquet app_input\data\02_processed\01_spot_report_data_cleaned.parquet
cp -r scripts app_input\scripts
cp -r models app_input\models
cp -r requirements.txt app_input\requirements.txt


# Upload the app_inputfolder to the AWS bucket "topspot"
