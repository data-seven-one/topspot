# importing general models
import os
import sys

# importing desired packages for python
import pandas as pd
import numpy as np
import warnings
from pandas.core.common import SettingWithCopyWarning
import lightgbm as lgb
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split, GridSearchCV, RandomizedSearchCV
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score, confusion_matrix, balanced_accuracy_score, roc_auc_score, classification_report

warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=SettingWithCopyWarning)

# global settings
CWD = os.path.abspath('.')
sys.path.append(CWD + '/scripts')
sys.path.append(CWD + '/scripts/classification')

try:
    import data_preparation as data_prep
    import data_cleaning as cleaning
except:
    print('there was an error importing the modules')

# display settings for output
pd.options.display.max_columns = 50
pd.options.display.max_rows = 1000

# Define functions for building test and training set and training the model
def build_test_and_training_set(df, features_to_use):
    X_all = df[features_to_use]
    y_all = df[['performance']]
    X_train,X_test,y_train,y_test=train_test_split(
                                                    X_all,
                                                    y_all,
                                                    test_size=0.1,
                                                    random_state=0,
                                                    stratify=y_all
                                                   )
    y_train.value_counts()
    y_test.value_counts()


    categorical_cols = X_all.columns[X_all.dtypes==object].tolist()
    for feature in categorical_cols:
            X_train[feature] = pd.Series(X_train[feature], dtype="category")
            X_test[feature] = pd.Series(X_test[feature], dtype="category")


    df_train=lgb.Dataset(X_train, y_train, categorical_feature='auto')
    df_test = lgb.Dataset(X_test, y_test, categorical_feature='auto')
    train = X_train.merge(y_train, left_index=True, right_index=True)
    test = X_test.merge(y_test, left_index=True, right_index=True)

    encoder = LabelEncoder()
    encoder.fit(y_train)
    y_train_encoded = encoder.transform(y_train)
    df_train_encoded = lgb.Dataset(X_train, y_train_encoded, categorical_feature='auto')

    return X_train, y_train_encoded, y_train, X_test, y_test, encoder


def train_and_evaluate_model(X_train, y_train_encoded):
    param_grid ={
                     'learning_rate' : [0.05],
                     'num_leaves':[10, 12],
                     'max_depth':[4, 5],
                     'min_child_samples': [60],
                     'min_child_weight': [0.01],
                     'num_iterations': [200],
                     'max_bin': [10],
                     'min_split_gain':[0.1],
                     'n_estimators':[40]
                    }
    param_fixed = {
                        'boosting_type':'gbdt',
                        'objective':'multiclass',
                        #'metric': 'average_precision',
                        #'is_unbalance':True,
                        #'max_depth':-1,
                        #'num_leaves':31,
                        #'subsample_for_bin':200000,
                        #'class_weight':'balanced',
                        #'min_child_weight':0.001,
                        #'min_child_samples':20,
                        'subsample':1.0,
                        #'importance_type':'split',
                        #'num_class': 2
                     }




    clf = lgb.LGBMClassifier(**param_fixed)
    #clf.get_params()
    gs = GridSearchCV(
                        estimator=clf,
                        param_grid = param_grid,
                        scoring=['precision_macro', 'recall_macro'],#, 'precision_macro', 'recall_macro'], #,'precision_macro',
                        cv=5,
                        return_train_score=True,
                        refit='precision_macro',
                        verbose=10
                      )
    #help(GridSearchCV)
    gs.fit(X_train, y_train_encoded)
    print('best achieved score is: {} %'.format(round(gs.best_score_, 4)*100))
    print('best parameter set is : {}'.format(gs.best_params_))
    #sorted(gs.cv_results_.keys())
    return gs


# Load raw data
df_raw = cleaning.load_process_and_store_data(load_new=False)

# Define whether to take percent or number of bins for classification
percent_or_nbins = 'n_bins'
if percent_or_nbins == 'percent':
    df_class = data_prep.bin_target_value_by_asset(df_raw, bins=[0, 0.333, 0.666, 1], labels=['low', 'middle', 'high'])
else:
    df_class = data_prep.bin_target_value_by_asset_not_percent(df_raw, n_bins=3, labels=['low','middle', 'high'], cut_off_date=pd.Timestamp('2020-04-01'))
    #df_class = data_prep.bin_target_by_spot(df_raw, n_bins=3, labels=['low','middle', 'high'], cut_off_date=pd.Timestamp('2020-04-01'))
df_class.performance.value_counts()

# Define seperate dataframes for each asset
df_amorelie = df_class[df_class['asset_name']=='AMORELIE']
df_jochen_schweizer = df_class[df_class['asset_name']=='JOCHEN SCHWEIZER']
df_flaconi = df_class[df_class['asset_name']=='FLACONI']
df_verivox = df_class[df_class['asset_name']=='VERIVOX']
df_moebelde = df_class[df_class['asset_name']=='MOEBEL.DE']
df_aroundhome = df_class[df_class['asset_name']=='AROUND HOME']
df_mydays = df_class[df_class['asset_name']=='MYDAYS']

features_for_model = [
                        # 'year',
                        # 'quarter',
                        # 'month',
                        # 'weekofyear',
                        #'dayofmonth',
                        'weekday',
                        'hour',
                        'is_prime_time',
                        'hauptwerbetraeger_name',
                        # 'tv_position_im_block',
                        # 'motiv_id',
                        # 'motiv_name',
                        # 'tv_wert',
                        'tv_spotlaenge',
                        # 'tv_spotanzahl_im_block',
                        # 'spot_id',
                        # 'tv_insel',
                        # 'relative_position',
                        # 'asset_name',
                        # 'react_per_cost',
                        # 'conv_per_cost',
                        # 'audience',
                        # 'ad_timestamp',
                        # 'total_react_linear',
                        # 'total_conv_linear',
                        # 'pc_react_linear',
                        # 'pc_conv_linear',
                        # 'mobile_react_linear',
                        # 'mobile_conv_linear',
                        # 'tablet_react_linear',
                        # 'tablet_conv_linear',
                        # 'if_wkz',
                        # 'partition_year',
                        # 'partition_month',
                        # 'partition_date',
                        # 'partition_adex_id',
                        # 'react_per_100_audience',
                        'relative_spot_position',
                        'spot_position_at_edge',
                        'day_time',
                        # 'performance'
                        ]
features_to_use = [
                    'weekday',
                    #'hour',
                    'is_prime_time',
                    'hauptwerbetraeger_name',
                    'tv_spotlaenge',
                    'relative_spot_position',
                    'spot_position_at_edge',
                    'day_time',
                    'asset_name'
                   ]

df = df_class.copy()
# df = df_flaconi.copy()
df.performance.value_counts()
X_train, y_train_encoded, y_train, X_test, y_test, encoder =  build_test_and_training_set(df, features_to_use)

gs = train_and_evaluate_model(X_train, y_train_encoded)
print('best achieved score is: {} %'.format(round(gs.best_score_, 4)*100))
print('best parameter set is : {}'.format(gs.best_params_))
best_model = gs.best_estimator_
predict_training = best_model.predict(X_train)
y_pred = encoder.inverse_transform(predict_training)


#balanced_accuracy_score(y_train, y_pred)
#f1_score(y_train, y_pred, average='weighted')
print(classification_report(y_true=y_train, y_pred=y_pred))

pd.Series(y_pred).value_counts()
y_train.value_counts()
pd.Series(y_pred).value_counts()
cm = confusion_matrix(y_true=y_train, y_pred=y_pred)


cm_matrix = pd.DataFrame(data=cm)
my_encodings_predicted = {l: 'predicted_{}'.format(i) for (l, i) in enumerate(encoder.fit(y_train).classes_)}
my_encodings_actual = {l: 'actual_{}'.format(i) for (l, i) in enumerate(encoder.fit(y_train).classes_)}
cm_matrix.rename(columns=my_encodings_predicted, index=my_encodings_actual)



imp=best_model.feature_importances_
feature_importances = pd.DataFrame(imp,index = X_train.columns,columns=['importance']).sort_values('importance',ascending=False)
plt.figure(figsize=(8,8))
plt.barh(feature_importances.index,feature_importances.importance,color='r')
plt.show()
