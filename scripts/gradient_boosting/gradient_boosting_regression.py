# importing general models
import os
import sys

# importing desired packages for python
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split, GridSearchCV
import shap
import lightgbm as lgb
from sklearn.metrics import mean_squared_error, mean_absolute_error
from math import sqrt
import pickle

# global settings
CWD = os.path.abspath('.')
sys.path.append(CWD + '/scripts')

try:
    import data_cleaning as cleaning
    import helper_functions as helper
except:
    print('there was an error importing the modules')

# display settings for output
pd.options.display.max_columns = 50
pd.options.display.max_rows = 1000


def load_data(CWD):
    folderpath_processed_data = CWD + '/data/02_processed'
    df_to_use = pd.read_parquet(folderpath_processed_data + '/01_spot_report_data_cleaned.parquet')

    return df_to_use


def mean_absolute_percentage_error(y_test, y_pred):
    y_test = np.where(y_true==0, 0.00000001, y_true)
    y_test, y_pred = np.array(y_test), np.array(y_pred)
    return np.mean(np.abs((y_test - y_pred) / y_test)) * 100


df = load_data(CWD)
# drop audience and total react linear, total conv total_conv_linear

df_features = df.copy()
df['react_per_10000_audience'] = df['react_per_100_audience']*100
features_for_model = [
                         #'ad_timestamp',
                         'weekday',
                         'hour',
                         'is_prime_time',
                         'hauptwerbetraeger_name',
                         'tv_position_im_block',
                         #'motiv_id',
                         #'motiv_name',
                         'tv_spotlaenge',
                         'tv_spotanzahl_im_block',
                         #'tv_insel',
                         'asset_name',
                         #'react_per_cost',
                         #'conv_per_cost',
                         #'audience',
                         'relative_spot_position',
                         'day_time',
                         'spot_position_at_edge'
                     ]
df_features = df_features[features_for_model]
# get categories and cast to string
categorical_cols = df_features.columns[df_features.dtypes==object].tolist()



# data split
X_train, X_test, y_train, y_test = train_test_split(
                                                        df[features_for_model],
                                                        df['react_per_10000_audience'],
                                                        test_size=0.3,
                                                        random_state=1
                                                    )

#Light GBM
for feature in categorical_cols:
    X_train[feature] = pd.Series(X_train[feature], dtype="category")
    X_test[feature] = pd.Series(X_test[feature], dtype="category")

train_data = lgb.Dataset(X_train, label=y_train, categorical_feature=categorical_cols, free_raw_data=False)
test_data = lgb.Dataset(X_test, label=y_test, categorical_feature=categorical_cols, free_raw_data=False)



regressor = lgb.LGBMRegressor()
params = {
            'boosting': ['gbdt'], # Normal gradient boosting decicion tree
            'objective': ['regression'], # regression problem
            #'metric': ['mae'], # metric used during evaluation of the training set
            # Regularization Parameters
            'num_leaves':[10,20], # Increasing will make model more complex (be aware of overfitting)
            'bagging_fraction': [1], # Only taking subsample to train each tree (speeds up trainig)
            'feature_fraction': [1], # Only taking subsample of features for each tree (speeds up training)
            'max_depth':[10], # depth of each trained tree (high value can cause overfitting)
            'max_bin': [15], # technique used to split values into discrete bins (histogram based algorithm)
            # small values can speed up training while large values improve accuracy

            # Training Parameters
            'num_iterations':[400,500], # number of boosting iterations (different trees) longer trainig, overfitting
            'learning_rate': [0.1], # Use small learning rate for large num_iterations
            #'early_stopping_rounds':[5], # stops training if its not doing anything useful anymore

            'categorical_feature':[categorical_cols],
            #'min_data_in_leaf':100,
            'bagging_freq': [5],
            'verbose': [0]
        }

gbm_reg = GridSearchCV(
                            regressor,
                            params,
                            #cv=3,
                            return_train_score=True,
                            scoring='neg_mean_absolute_error'
                        )
#
gbm_reg.fit(X_train, y_train)

gbm_reg.best_params_
gbm_reg.best_score_
model = gbm_reg.best_estimator_

y_pred_train = model.predict(X_train)
# eval
print('The mean squared error of prediction is:', mean_squared_error(y_train, y_pred_train))
from sklearn.metrics import mean_absolute_error
print('The mean absolute error of prediction is:', mean_absolute_error(y_train, y_pred_train))
print('The root mean absolute error of prediction is:', sqrt(mean_absolute_error(y_train, y_pred_train)))


# predict
y_pred = model.predict(X_test)
# eval
print('The mean squared error of prediction is:', mean_squared_error(y_test, y_pred))
from sklearn.metrics import mean_absolute_error
print('The mean absolute error of prediction is:', mean_absolute_error(y_test, y_pred))
print('The root mean absolute error of prediction is:', sqrt(mean_absolute_error(y_test, y_pred)))

y_compare = X_test.copy()
y_compare['y_predict'] = list(y_pred)
y_compare['y_real'] = list(y_test)
#make all 0 values to 1
y_compare['y_real'] = list(y_test)
#y_compare['y_real'] = np.where(y_compare['y_real']==0, 1, y_compare['y_real'])

y_compare['abs_diff'] = abs(y_compare['y_real'] - y_compare['y_predict'])
y_compare['abs_diff_squared'] = (y_compare['abs_diff'] * y_compare['abs_diff'])
y_compare['perc_diff'] = ((y_compare['abs_diff'])/((y_compare['y_real'] + y_compare['y_predict'])/2))
y_compare['percent_error'] = abs((y_compare['y_real'] - y_compare['y_predict']) / y_compare['y_real'])
mean_absolute_error = (y_compare['abs_diff'].mean())
rmse = (sqrt(y_compare['abs_diff_squared'].mean()))
mape =(y_compare['percent_error'].mean())





# Store model
storing_model_path = CWD + '/models/all/model.sav'
storing_X_train_path = CWD + '/models/all/X_train.parquet'
detailed_shap_fig_path = CWD + '/models/all/detailed_shap.jpeg'
helper.save_model_as_sav(storing_model_path, model)
X_train.to_parquet(storing_X_train_path)


X_train_all = pd.read_parquet(storing_X_train_path)
# Load model
model_all = helper.load_model(storing_model_path)
#

# Make Shapley Values
shap_values = helper.make_shap_value(model_all, X_train_all)
detailed_view = helper.make_detailed_shap_plot(shap_values, X_train, saving_path)
feature_importance = helper.make_feature_importance_shap_plot(shap_values, X_train)
dependence_features = helper.make_dependence_shap_plot(shap_values, X_train, 'asset_name')
